﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

IF (SELECT COUNT(*) FROM Colors) = 0
	INSERT INTO Colors VALUES ('#fff'), ('#ccc')

IF (SELECT COUNT(*) FROM Repeat) = 0
	INSERT INTO Repeat VALUES (0, 'No-repeat'), (1, 'Every day'), (7, 'Every week'), (30, 'Every month')

IF (SELECT COUNT(*) FROM Access) = 0
	INSERT INTO Access VALUES ('Private'), ('Public')

IF (SELECT COUNT(*) FROM Users) = 0
	BEGIN
		EXEC uspCreateUser 'username001', 'email@test', 'https://images.com/1'
		EXEC uspCreateUser 'username002', 'email2@test', 'https://images.com/2'
	END


IF (SELECT COUNT(*) FROM Events) = 0
	BEGIN
		DECLARE @lastCalendarId int = (SELECT TOP 1 Id FROM Calendars ORDER BY Id DESC)
		EXEC uspCreateEvent @lastCalendarId, '', 'dnipro js meet-up', '2019-11-02 18:00:00', '2019-11-02 21:30:00', 0, 0
	END