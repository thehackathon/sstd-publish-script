﻿CREATE PROCEDURE [dbo].[uspCreateCalendar]
	@userId int,
	@name nvarchar(max),
	@accessId int,
	@colorId int
AS
BEGIN
DECLARE @CalendarUsers CalendarUsers
DECLARE @lastId int
SET NOCOUNT ON;
BEGIN TRANSACTION Transact
	BEGIN TRY
		INSERT INTO [Calendars] (Name, AccessId, OwnerId, ColorId)
		VALUES (@name, @accessId, @userId, @colorId)

		SET @lastId = SCOPE_IDENTITY()

		INSERT INTO @CalendarUsers (userId, calendarId)
		VALUES (@userId, @lastId)

		EXEC uspSetCalendarsToUser @CalendarUsers
		COMMIT TRANSACTION Transact
	END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION Transact
END CATCH  
END