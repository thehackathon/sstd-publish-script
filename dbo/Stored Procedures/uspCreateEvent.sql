﻿CREATE PROCEDURE [dbo].[uspCreateEvent]
	@calendarId int, 
	@description nvarchar(max),
	@title nvarchar(max),
	@timeStart datetime,
	@timeFinish datetime, 
	@isAllDay bit,
	@repeatId int
AS
BEGIN
	BEGIN TRANSACTION Transact
		BEGIN TRY
			INSERT INTO [Events] (CalendarId, Description, Title, TimeStart, TimeFinish, AllDay, RepeatId)
			VALUES (@calendarId, @description, @title, @timeStart, @timeFinish,@isAllDay, @repeatId)
			SELECT CAST(SCOPE_IDENTITY() AS INT)
		COMMIT TRANSACTION Transact
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION Transact
	END CATCH  
END
