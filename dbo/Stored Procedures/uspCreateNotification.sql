﻿CREATE PROCEDURE [dbo].[uspCreateNotification]
	@eventId int,
	@minutesBefore int
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [Notification] (EventId, MinutesBefore)
	VALUES (@eventId, @minutesBefore)
	RETURN (SELECT SCOPE_IDENTITY())
END
