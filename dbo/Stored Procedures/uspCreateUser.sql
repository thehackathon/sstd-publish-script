﻿CREATE PROCEDURE [dbo].[uspCreateUser]
	@name nvarchar(max),
	@email nvarchar(max),
	@picture nvarchar(max)
AS
BEGIN
	declare @userId int
	BEGIN TRANSACTION Transact
		BEGIN TRY
			INSERT INTO [Users] (Name, Email, Picture)
			VALUES (@name, @email, @picture)

			SET @userId = SCOPE_IDENTITY()

			EXEC uspCreateCalendar @userId, 'Default', 1, 1
			COMMIT TRANSACTION Transact
		END TRY
			BEGIN CATCH
		ROLLBACK TRANSACTION Transact
	END CATCH  
END
