﻿
CREATE PROCEDURE [dbo].[uspGeEvents] 
	@userId int,
	@calendarIds CalendarIds null readonly, 
	@dateTimeStart datetime,
	@dateTimeFinish datetime
AS
BEGIN
	SELECT uc.CalendarId, ca.Name AS CalendarName, a.Name AS AccessName, 
	e.Id AS EventId, e.Description, e.Title,
	e.TimeStart, e.TimeFinish, e.AllDay

	FROM [Users] u
	LEFT JOIN [UserCalendars] uc ON u.Id = uc.UserId
	LEFT JOIN [Calendars] ca ON uc.CalendarId = ca.Id
	LEFT JOIN [Access] a ON a.Id = ca.AccessId
	LEFT JOIN [Events] e ON e.CalendarId = ca.Id

	WHERE u.Id = @userId
	AND @dateTimeStart <= e.TimeFinish
	AND @dateTimeFinish >= e.TimeStart
	AND ca.Id IN (SELECT * FROM @calendarIds)
END
