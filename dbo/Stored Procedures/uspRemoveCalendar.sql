﻿CREATE PROCEDURE [dbo].[uspRemoveCalendar]
	@calendarId int
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION Transact
		BEGIN TRY

			DELETE FROM [UserCalendars]
			WHERE CalendarId = @calendarId

			DELETE FROM [Notification]
			WHERE EventId IN (SELECT Id FROM [Events] WHERE CalendarId = @calendarId)

			DELETE FROM [Events] WHERE CalendarId = @calendarId

			DELETE FROM [Calendars]
			WHERE Calendars.Id = @calendarId

			COMMIT TRANSACTION Transact
		END TRY
	BEGIN CATCH
		  ROLLBACK TRANSACTION Transact
	END CATCH
END
