﻿
CREATE PROCEDURE [dbo].[uspSetCalendarsToUser]
	@calendarUsers CalendarUsers readonly
AS
BEGIN
	SET NOCOUNT ON;
    INSERT INTO UserCalendars (UserId, CalendarId)
	select userId, calendarId from @calendarUsers

	SELECT SCOPE_IDENTITY()
END
