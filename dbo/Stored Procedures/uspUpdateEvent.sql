﻿CREATE PROCEDURE [dbo].[uspUpdateEvent]
	@id int,
	@calendarId int, 
	@description nvarchar(max) null,
	@title nvarchar(max),
	@repeatId int,
	@timeStart datetime null,
	@timeFinish datetime null, 
	@isAllDay bit null
AS
BEGIN
	BEGIN TRANSACTION Transact
  BEGIN TRY
	UPDATE [Events]
	SET 
	CalendarId = @calendarId, 
	Description = @description, 
	Title = @title, 
	TimeStart = @timeStart, 
	TimeFinish = @timeFinish,
	AllDay = @isAllDay,
	RepeatId = @repeatId
	where Id = @id
  COMMIT TRANSACTION Transact
  END TRY
  BEGIN CATCH
      ROLLBACK TRANSACTION Transact
  END CATCH  
END
