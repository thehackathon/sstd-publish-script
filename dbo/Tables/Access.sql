﻿CREATE TABLE [dbo].[Access] (
    [Id]   INT           NOT NULL IDENTITY (1, 1),
	[Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Access] PRIMARY KEY CLUSTERED ([Id] ASC)
);

