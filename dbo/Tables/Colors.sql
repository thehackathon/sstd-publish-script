﻿CREATE TABLE [dbo].[Colors] (
    [Id]  INT            NOT NULL IDENTITY (1, 1),
    [Hex] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Color] PRIMARY KEY CLUSTERED ([Id] ASC)
);

