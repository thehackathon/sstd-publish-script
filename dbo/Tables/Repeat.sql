﻿CREATE TABLE [dbo].[Repeat] (
    [Id]       INT        NOT NULL,
	[Interval] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Repeat] PRIMARY KEY CLUSTERED ([Id] ASC)
);

